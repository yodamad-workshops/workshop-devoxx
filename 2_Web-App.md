

## Un projet plus réaliste

Maintenant que nous avons les bases, nous allons utiliser un projet de démo que nous avons préparer pour découvrir des fonctionnalités plus avancées.

* Forker le projet [web-app](https://gitlab.com/gitlabuniversity/web-app) via le bouton `Fork`
* Regarder le `.gitlab-ci.yml`, il inclut:
    * des notions connues : *rules*, *stage*, *environment*
    * 2 nouvelles notions :
        * `include`: permet d'importer un autre fichier `yml` contenant des définitions de stages et de jobs ([doc officielle](https://docs.gitlab.com/ee/ci/yaml/#include))
        * `extends`: permet de surcharger un job déjà défini. Cela peut être un job  *abstrait* que l'on préfixe avec `.`  ([doc officielle](https://docs.gitlab.com/ee/ci/yaml/#extends))

    👉 Ces 2 notions sont pratiques lorsque l'on souhaite mettre en place une bibliothèque de jobs réutilisables et/ou normaliser ses pipelines, mais aussi pour découper les jobs en plusieurs fichiers pour que cela reste lisible lorsque l'on a des pipelines avec beaucoup de jobs.

* Déclencher un pipeline manuellement depuis `CI/CD > Pipelines` et le bouton `Run Pipeline`
* Vérifier que l'environnement est bien créé et que l'application est déployée en cliquant sur le bouton `Open`

```
✅ Le site est déployé et accessible depuis le menu Environments avec le nom `xxx-web-app-main`
```

Il serait intéressant de pouvoir avoir un environnement par Merge Request afin de valider des modifications avant de valider celle-ci.

### Review app

Pour cela, GitLab fournit le mécanisme de `review app` ([doc officielle](https://docs.gitlab.com/ee/ci/review_apps/)) qui sont des environnements dynamiques.

Le projet `web-app` contient déjà les mécanismes dans le CI pour gérer cela (via la variable `DYNAMIC_ENVIRONMENT_URL`, cf le fichier dans le répertoire `ci/pages/`).

Désormais:
* créer une branche `review-app` et la MR associée (***attention*** à pointer sur la branche `main` de **votre projet** et pas de celle du projet d'origine qui est sélectionnée par défaut)
* sur cette branche, modifier le fichier `main.go` dans le répertoire `website` (✋ pas besoin de connaitre le go pour faire la modification 😅)
    * Par exemple, remplacer `👋 Hello World 🌍` par `👋 Hello Devoxx 🗼`
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Un environnement a été ajouté dans la liste
✅ Le site modifié est déployé à l'URL liée à l'environnement
✅ L'environnement est affiché dans la MR
```

* merger la MR dans `main`

```
✅ Le pipeline s'exécute correctement
✅ L'environnement de la MR a été supprimé
✅ Le site de la branche main est mis à jour
```

### Gestion des tests unitaires

Dans les vrais projets, nous avons tous des tests unitaires. GitLab permet d'avoir des [rapports sur les tests unitaires](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html) à différents niveaux.

Dans le projet en cours sur la branche `main`:
* ajouter à la racine du répertoire, un fichier de tests unitaires (en JS par exemple, [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519499))
* ajouter un job dans `.gitlab-ci.yml` pour exécuter ces tests
    * ce job doit propduire un *artifact* avec le fichier de résultats des tests `test-results.xml`
    * GitLab fournit des mots clés pour reconnaitre des [rapports](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html)
    * [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519501) pour vous aider à démarrer avec le job
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Dans l'onglet du pipeline, le nombre de tests unitaires est affiché et tous sont ok
```

* créer une branche et une MR `failing-test` (***attention*** à pointer sur la branche `main` de **votre projet** et pas de celle du projet d'origine qui est sélectionnée par défaut)
* sur cette branche, ajouter un nouveau test dans `tests.js` qui ne passe pas
    * 💡 [exemple](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519500)
* commiter

```
✋ Le pipeline est en erreur
✅ Dans la MR, le résumé des tests apparait avec le test en erreur
```

* corriger le test en erreur
* commiter

```
✅ Le pipeline s'exécute correctement
✅ Dans la MR, le résumé des tests apparait sans test en erreur
```

### Code quality

GitLab fournit également des analyseurs de code pour contrôler la qualité, en se basant sur l'outil [CodeClimate](https://codeclimate.com/).

Dans le projet en cours sur la branche `main`:
* configurer code climate pour go en utilisant le [snippet](https://gitlab.com/gitlabuniversity/workshop-snippets/-/snippets/2519502) fourni
    * il existe d'autres [analyzers](https://docs.gitlab.com/ee/ci/testing/code_quality.html#using-analysis-plugins) en fonction du langage de programmation utilisés
* ajouter un job pour contrôler la qualité du code go en utilisant les [mécanismes fournis par GitLab](https://docs.gitlab.com/ee/ci/testing/code_quality.html)
    * Penser à ce que le job de code quality s'exécute sur les merges request (*rules*)
* commiter

```
✅ Le pipeline s'exécute correctement
```

On constate que le job `code-quality` met un certain à s'exécuter par défaut. GitLab permet via la notion de [label](https://docs.gitlab.com/ee/ci/yaml/#tags) de pouvoir typer les jobs. Cela sert en particulier pour que les jobs soient exécutés sur des runners en particulier, comme par exemple certains [shared runners](https://docs.gitlab.com/ee/ci/runners/) mis à disposition sur gitlab.com.

* modifier le job `code-quality` pour qu'il soit pris en charge par un [runner `medium`](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html)
* commiter sur la branche main

```
✅ Le pipeline s'exécute correctement
✅ Le job code-quality s'exécute plus rapidement
✅ Le job a été exécuté par un shared runner medium
```

On peut également avoir le rapport de Code Quality au sein d'une merge request.

* créer une branche & une MR `code-quality` (***attention*** à pointer sur la branche `main` de **votre projet** et pas de celle du projet d'origine qui est sélectionnée par défaut)
* modifier le code pour ajouter du code moche (par exemple du code dupliqué)
* commiter sur la branche

```
✅ Le pipeline s'exécute correctement
✅ Le panel Code Quality apparait dans la MR
```
